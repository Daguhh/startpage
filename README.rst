=========
StartPage
=========

A simple config file to generate a "terminal like" webbrowser startpage.

The generated page is intended to mimic the one proposed by `lokesh-krishna <https://github.com/lokesh-krishna/lokesh-krishna.github.io>`_ and use a lightly modified version of its files. You can find a demo of the original page here : https://lokesh-krishna.github.io/

But instead of working on raw html, purpose here is to propose a convinient way to personnalise entries of such a page :

Demo
----

.. image:: StartPage.png
   :width: 250 px
   :target: https://daguhh.frama.io/startpage/


Generate
--------

Start page generation take three step:

1. firstly, create a file named **index.ini**.
Here is the file structure, but there's an example in the repo.

.. code::

    [Section_1]
    entry_1=https://www.example.com/
    entry_2=https://www.example.com/

    [Section_2]
    entry_1=https://www.example.com/
    entry_2=https://www.example.com/
    ...

2. then, generate a reStructuredText file (index.rst)

.. code:: bash

    python3 create_rst.py 

3. finally, convert this file into html with **rst2html5** command from **python3-docutils** package :

.. code:: bash

    rst2html5 --title=StartPage --stylesheet-path=terminal.css index.rst > index.html

Activate
--------

Then go to your webbrowser preferences and change start page to the generated **index.html** file.

For firefox, you can enter **about:preferences#home** in the search bar to directly access the config page.

Customize
---------

The user can, at each step, personalize its startpage, by modifying either :

- index.ini : this is the simplest way to do to add new entries
- index.rst 
- index.html
- terminal.css : to change style sheet


