#!/usr/bin/env python3

from configparser import ConfigParser

config = ConfigParser()
config.read('index.ini')

SECTION_head="""    :directory:`~`

    :directory:`firefox:~$` :green-text:`ls`

    .. container:: links

        {}

    """

SECTION="""    :directory:`~`

    .. _{}:

    :directory:`firefox:~$` :green-text:`ls` :argument:`{}`

    .. container:: links

        {}

    """

LINK = """.. _{}: {}\n"""

link_ref=[]

text='''
.. role:: spaced-link
.. role:: links
.. role:: directory
.. role:: blue-text
.. role:: green-text
.. role:: magenta-text
.. role:: green-text
.. role:: argument
.. role:: terminal

.. container:: terminal
    :name: terminal

'''

link_line = ''
for directory in config.sections():
    link_line += f'{directory}_ '

text+=SECTION_head.format(link_line)
urls = []
urls_name = []
for directory in config.sections():
    link_line = ''
    for item in config[directory]:
        urls += [config[directory][item]]
        urls_name += [item]
        link_line += f'{item}_ '

    text+=SECTION.format(directory, directory, link_line)

text += '\n'

for name, url in zip(urls_name, urls):

    text += LINK.format(name, url)


with open('index.rst', 'w') as f:
    f.write(text)

